package accounting.virtualcard.application;

import lombok.extern.slf4j.Slf4j;
import accounting.user.UserRepo;
import accounting.virtualcard.domain.VirtualCreditCardRepo;

import javax.transaction.Transactional;

//User can blacklisted when hee is activated
//User can be activated when he is not activated
//User can changeNickname when he is not deactivated



//application service - should be one application service per use case!
//port agnostic (nothing to do with REST, queues, command lines, etc)
// ports (REST, queues, command lines) inject this service and adapt request to match activateCard() method
@Slf4j
public class ActivateCardService {

    private final VirtualCreditCardRepo virtualCreditCardRepo;
    private final UserRepo userRepo;

    public ActivateCardService(VirtualCreditCardRepo virtualCreditCardRepo, UserRepo userRepo) {
        this.virtualCreditCardRepo = virtualCreditCardRepo;
        this.userRepo = userRepo;
    }

    @Transactional
    public void activateCard() {
        log.info("application service is a place to log stuff");
        // checkSecurity()
        // accounting.virtualcard.domain = create card
        // accounting.domain - create user
        // accounting.virtualcard.infrastrucre -> persist card  (accessed by interface)
        // users.infrastrucre -> persist user  (accessed by interface)

    }
}
