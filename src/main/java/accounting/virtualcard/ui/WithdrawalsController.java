package accounting.virtualcard.ui;

import accounting.withdrawal.application.WithdrawalsService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.HttpStatus.FORBIDDEN;

@RestController
// port and adapter in one class
// (port via rest, adapter via spring - json to WithdrawalCommand)
public class WithdrawalsController {

    private final WithdrawalsService withdrawalsService;

    public WithdrawalsController(WithdrawalsService withdrawalsService) {
        this.withdrawalsService = withdrawalsService;
    }

    @PostMapping("/withdrawals")
    public ResponseEntity withdraw(WithdrawalCommand cmd) {
        validateRequest(); // or spring can do that
        boolean result = withdrawFromServie();
        // adapt response to port language (in this case http codes)
        if(result){
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.status(FORBIDDEN).build();
        }
    }

    private boolean withdrawFromServie() {
        return false; // return withdrawalService.withdraw()
    }

    private void validateRequest() {

    }


}

class WithdrawalCommand {
}
