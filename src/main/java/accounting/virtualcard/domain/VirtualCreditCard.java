package accounting.virtualcard.domain;


import java.math.BigDecimal;

public class VirtualCreditCard {
    private boolean active = true;
    private int withdrawalsInCurrentCycle = 0;
    private final BigDecimal limit;
    private BigDecimal used = BigDecimal.ZERO;

    public VirtualCreditCard(BigDecimal limit) {
        this.limit = limit;
    }

    public boolean withdraw(BigDecimal amount) {
        if (isAvailable(amount)) {
            used = used.add(amount);
            withdrawalsInCurrentCycle++;
            return true;
        }
        return false;
    }

    private boolean isAvailable(BigDecimal amount) {
        return isActive() && isWithinLimit(amount) && canWithdrawInCycle();
    }

    private boolean canWithdrawInCycle() {
        return withdrawalsInCurrentCycle < 45;
    }

    private boolean isWithinLimit(BigDecimal amount) {
        return availableLimit().compareTo(amount) >= 0;
    }

    private BigDecimal availableLimit() {
        return limit.subtract(used);
    }

    private boolean isActive() {
        return active;
    }

    public void deactivate() {
        active = false;
    }
}