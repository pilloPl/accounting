package accounting.virtualcard.domain;


import java.math.BigDecimal;

public interface WithdrawalFeeCalculatorPolicy {
}



interface FeeModifier extends WithdrawalFeeCalculatorPolicy {
    Fee modify(Fee fee);
}

class ConstantModifier implements FeeModifier {

    private final BigDecimal constantFee = new BigDecimal(5);

    @Override
    public Fee modify(Fee fee) {
        return fee.modifyTo(constantFee);
    }
}

//class SuperCoolConfigrableModifier implements FeeModifier {
//
//    Validator validator;
//    Applyier applyier;
//    Guardian guardian;
//
//
//    @Override
//    public Fee modify(Fee fee) {
//        if(validator.passes(fee)) {
//            Fee newFee = applier.applyTo(fee);
//            if(guardian.passes(newFee)) {
//                return newFee;
//            }
//            return fee;
//        }
//    }
//}
//
//



class Fee {
    final BigDecimal feeAmount;
    final BigDecimal withdrawalAmount;

    Fee(BigDecimal feeAmount, BigDecimal withdrawalAmount) {
        this.feeAmount = feeAmount;
        this.withdrawalAmount = withdrawalAmount;
    }


    Fee modifyTo(BigDecimal feeAmount) {
        return new Fee(withdrawalAmount, feeAmount);
    }
}
