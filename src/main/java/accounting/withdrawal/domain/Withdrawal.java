package accounting.withdrawal.domain;

import lombok.Data;

import java.math.BigDecimal;
import java.time.Instant;

@Data
public class Withdrawal {

    private Instant when;
    private Long billingCycleId;
    private BigDecimal howMuch;

    public BigDecimal getHowMuch() {
        return howMuch;
    }
}
