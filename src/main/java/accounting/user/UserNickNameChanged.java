package accounting.user;

import java.time.Instant;

/**
 * Created by pilo on 06.10.17.
 */
public class UserNickNameChanged implements DomainEvent {

    private final String newNickname;
    private final Instant timestamp;

    public UserNickNameChanged(String newNickname, Instant timestamp) {
        this.newNickname = newNickname;
        this.timestamp = timestamp;
    }

    public String getNewNickname() {
        return newNickname;
    }

    @Override
    public Instant occuredAt() {
        return timestamp;
    }
}
