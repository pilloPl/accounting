package accounting.user;


import java.util.UUID;

public interface UserRepo {

    User findUser(UUID userId);

    void save(User user);
}