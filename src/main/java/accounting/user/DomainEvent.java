package accounting.user;

import java.time.Instant;

/**
 * Created by pilo on 06.10.17.
 */
public interface DomainEvent {

    Instant occuredAt();
}
