package accounting.user;

import java.time.Instant;

/**
 * Created by pilo on 06.10.17.
 */
public class UserActivated implements DomainEvent {

    private Instant now;

    public UserActivated(Instant now) {
        this.now = now;
    }

    @Override
    public Instant occuredAt() {
        return now;
    }
}
