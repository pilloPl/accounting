package accounting.user;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by pilo on 06.10.17.
 */
public class InMemoryUserRepo implements UserRepo {

    private final Map<UUID, User> users = new ConcurrentHashMap<>();
    @Override
    public User findUser(UUID userId) {
        return users.get(userId);
    }

    @Override
    public void save(User user) {
        users.put(user.getUUID(), user);
    }
}
