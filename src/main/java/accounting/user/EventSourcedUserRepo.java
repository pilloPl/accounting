package accounting.user;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by pilo on 06.10.17.
 */
public class EventSourcedUserRepo implements UserRepo {

    private final Map<UUID, List<DomainEvent>> users = new ConcurrentHashMap<>();
    @Override
    public User findUser(UUID userId) {
        return User.createFrom(userId, users.get(userId));
    }

    @Override
    public void save(User user) {
        List<DomainEvent> currentEvents = users.getOrDefault(user.getUUID(), new ArrayList<>());
        List<DomainEvent> newEvents = user.getUncommitedEvents();
        currentEvents.addAll(newEvents);
        users.put(user.getUUID(), currentEvents);
        user.flushChanges();
    }
}
