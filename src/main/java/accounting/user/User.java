package accounting.user;

//User can blacklisted when hee is activated
//User can be activated when he is not activated
//User can change nickname when he is not deactivated


import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class User {

    private final UUID uuid;
    private boolean active;
    private boolean blacklist;
    private String nickname = "";
    private List<DomainEvent> changes = new ArrayList<>();
    private java.util.UUID UUID;

    public User(UUID uuid) {
        this.uuid = uuid;
    }

    public void changeNickName(String newNick) { //command (beaviour)
        if(isDeactivated()) { //invariant
            throw new IllegalStateException(); //NACK
        } //ACK (it happened!)
        nicknameChanged(new UserNickNameChanged(newNick, Instant.now()));
    }

    private User nicknameChanged(UserNickNameChanged userNickNameChanged) {
        nickname = userNickNameChanged.getNewNickname();
        changes.add(userNickNameChanged);
        return this;
    }

    public void activate() {
        if(isActivated()) {
            throw new IllegalStateException();
        }
        userActivated(new UserActivated(Instant.now()));
    }

    private User userActivated(UserActivated userActivated) {
        active = true;
        changes.add(userActivated);
        return this;
    }

    public void addToBlackList() {
        if (!isActivated()) {
            throw new IllegalStateException();
        }
        userMarkedToBlackList(new UserMarkedToBlacklist(Instant.now()));
    }

    private User userMarkedToBlackList(UserMarkedToBlacklist userMarkedToBlacklist) {
        blacklist = true;
        changes.add(userMarkedToBlacklist);
        return this;
    }

    public boolean isActivated() {
        return active;
    }


    public boolean isDeactivated() {
        return !isActivated();
    }

    public String getNickname() {
        return nickname;
    }

    public boolean isBlacklisted() {
        return blacklist;
    }

    public UUID getUUID() {
        return uuid;
    }

    public List<DomainEvent> getUncommitedEvents() {
        return changes;
    }

    public void flushChanges() {
        changes.clear();
    }

    public static User createFrom(UUID uuid, List<DomainEvent> domainEvents) {
        User initialState = new User(uuid);
        return javaslang.collection.List.ofAll(domainEvents).foldLeft(
                initialState, User::handleEvent);
    }
    public User handleEvent(DomainEvent event) {
        if (event instanceof UserNickNameChanged) {
            return nicknameChanged((UserNickNameChanged) event);
        }

        if (event instanceof UserActivated) {
            return userActivated((UserActivated) event);
        }

        if (event instanceof UserMarkedToBlacklist) {
            return userMarkedToBlackList((UserMarkedToBlacklist) event);
        }

        throw new IllegalStateException();
    }

}
