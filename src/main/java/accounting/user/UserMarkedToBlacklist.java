package accounting.user;

import java.time.Instant;

/**
 * Created by pilo on 06.10.17.
 */
public class UserMarkedToBlacklist implements  DomainEvent {

    private Instant now;

    public UserMarkedToBlacklist(Instant now) {
        this.now = now;
    }

    @Override
    public Instant occuredAt() {
        return now;
    }
}
