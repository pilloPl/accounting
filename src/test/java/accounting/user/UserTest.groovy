package accounting.user

import spock.lang.Specification

//User can blacklisted when hee is activated
//User can be activated when he is not activated
//User can change nickname when he is not deactivated

class UserTest extends Specification {

    User user = new User(UUID.randomUUID())

    def "User can be blacklisted when he is activated"() {
        given:
            user.activate()
        when:
            user.addToBlackList()
        then:
            user.isBlacklisted()
    }

    def "User cannot be blacklisted when he is not activated"() {
        when:
            user.addToBlackList()
        then:
            thrown(IllegalStateException)
    }

    def "User can be activated when he is not activated"() {
        when:
            user.activate()
        then:
            user.isActivated()
    }

    def "User cannot be activated when he is activated"() {
        given:
            user.activate()
        when:
            user.activate()
        then:
            thrown(IllegalStateException)
    }

    def "User can change nickname when he is activated"() {
        given:
            user.activate()
        when:
            user.changeNickName("nickName")
        then:
            user.getNickname() == "nickName"
    }

    def "User cannot change nickname when he is deactivated"() {
        when:
            user.changeNickName("nickName")
        then:
            thrown(IllegalStateException)
    }
}
