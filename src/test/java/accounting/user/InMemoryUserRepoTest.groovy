package accounting.user

import spock.lang.Specification

/**
 * Created by pilo on 06.10.17.
 */
class InMemoryUserRepoTest extends Specification {

    UserRepo userRepo = new EventSourcedUserRepo()

    def 'should save and load user'() {
        given:
            UUID uuid = UUID.randomUUID()
        and:
            User user = new User(uuid)
        and:
            user.activate()
        and:
            user.changeNickName("Michal")
        when:
            userRepo.save(user)
        and:
            User loaded = userRepo.findUser(uuid)
        then:
            loaded.isActivated()
            loaded.getNickname() == "Michal"
    }
}
