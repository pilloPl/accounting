package accounting.virtualcard.domain

import spock.lang.Specification
import spock.lang.Unroll

/**
 * Created by pilo on 05.10.17.
 */
class VirtualCreditCardTest extends Specification {


    def "vcc should be active in order to withdraw"() {
        given:
            VirtualCreditCard card = new VirtualCreditCard(100.00)
        expect:
            card.withdraw(100.00)

    }

    def "cannot withdraw when it is deactivated"() {
        given:
            VirtualCreditCard card = new VirtualCreditCard(100.00)
        and:
            card.deactivate()
        expect:
            !card.withdraw(100.00)

    }

    def "cannot withdraw when too less hajs"() {
        given:
            VirtualCreditCard card = new VirtualCreditCard(100.00)
        expect:
            !card.withdraw(105.00)

    }

    def "cannot withdraw when too less hajs after 1st accounting.withdrawal"() {
        given:
            VirtualCreditCard card = new VirtualCreditCard(100.00)
        and:
            card.withdraw(100.00)
        expect:
            !card.withdraw(10.00)

    }

    @Unroll
    def "can (#expectedResult) withdraw in cycle after #nrOfTimes withdrawals"() {
        given:
            VirtualCreditCard card = new VirtualCreditCard(100.00)
        and:
            nrOfTimes.times { card.withdraw(1.00) }
        expect:
            card.withdraw(10.00) == expectedResult
        where:
            nrOfTimes || expectedResult
            2         || true
            5         || true
            25        || true
            44        || true
            45        || false



    }

}
